----------
### 项目简介
- 综合运用slim图片分类和object-detection框架，实现对车辆型号识别及位置检测
- 提供一个车辆分类类别的数据集，包括764类，分类参考如下： 
 - 0:Jeep-北京JEEP
 - 1:Jeep-大切诺基
 - 2:Jeep-大切诺基SRT
 - 3:Jeep-指南者
 - ...
- 数据集包括48856张图片 其43971张作为训练集，4885张作为验证集，数据已经预先打包成tfrecord格式
  - tfrecord数据文件是一种将图像数据和标签统一存储的二进制文件
  - 能更好的利用内存，在tensorflow中快速的复制，移动，读取，存储等
- 问题分析
  - 项目数据为分类数据，可以作为图片车辆分类训练用
  - 缺少位置信息：1. 需要包括车辆位置信息的数据 2. imagenet训练好的检测模型
- 解决思路：
 - 分为检测和分类两部分
 - 端到端的方式：yolo、ssd等 
- 查阅资料
  - YOLO:You Only Look Once
  - YOLO直接从一张图片中提取特征，来预测每一个Bounding box，最小化和ground turth的误差。
  - 核心是将图片划分为S×S的网格，每一个网格中预测B个Bounding box 和confidence score。如果每一个网格中有物体存在，那么会得到confidence score如果不存在物体，则为0。物体的位置用(x,y,w,h)表示。
![](https://i.imgur.com/JjF9CQL.png)
  - SSD： Single Shot MultiBox Detector
  - 核心就是将feature map切分成8×8或者4×4之后的一个个格子，在每个格子上有一系列不同aspect ratio的boxes，用于检测物体以及归属类别得分。
![](https://i.imgur.com/ujA2y7C.png)

- 初步方案：分为检测和分类两部分
 - 检测：用pascal voc2012数据在object_detection框架训练
 - 分类：直接项目提供的数据在slim图片分类框架训练

----------

### 模型训练

- 分类数据已经给出，可以直接在第八周课程的基础上直接开始训练
![](https://i.imgur.com/ZchTRgv.png)
- 模型训练
  - 调用GitHub上从tensorflow官方fork的slim框架代码，代码基本和第八周相似
 
  - 用inceptionv4预训练模型初始化
  - ![](https://i.imgur.com/zaBUn3F.png)






- 检测模型需要有位置信息的数据集，下载pascal voc2012，在tensorflow的models中直接基础上修改create_pascal_tf_record.py文件，运行即可生成tf所需tfrecord格式数据，具体修改：
  - 修改pascal label路径：`flags.DEFINE_string('label_map_path', '/home/ai/model_1/pas.txt'，'Path to label map proto')`
  - 修改数据目录名称，默认aeroplane改成car：`examples_path = os.path.join(data_dir, year, 'ImageSets', 'Main', 'car_' + FLAGS.set + '.txt')`
![](https://i.imgur.com/UFBGTJ4.png)
  - 模型使用mobilenet模型ssd检测框架，在model_zoo下载预训练模型并上传tinymind的pretrain数据集下
![](https://i.imgur.com/rZmhQpf.png)









----------


###训练效果

-  分类模型效果

  - 分类模型训练在tinymind平台用1GPU训练27小时
![](https://i.imgur.com/Klgo6QM.png)
  - 可以看到准确率达到0.827 
  - 保存训练后权重
![](https://i.imgur.com/EOkz60X.png)

 
  - 在网上随便下的两张图测试训练后的分类模型
![](https://i.imgur.com/zP21L15.png)

  - 可以看到模型准确的识别出了路虎-揽胜

![](https://i.imgur.com/LjMBtSE.png)

   - 北京-JEEP
  
----------

-  检测模型效果

  - 在检测模型训练至step8500时候终止，loss从70降至2左右
![](https://i.imgur.com/GY6G9j7.png)
----------
  - 保存下检测模型训练权重

----------

![](https://i.imgur.com/l1bFTVO.png)

----------

  - 在汽车之家找的图片，日产蓝鸟可以识别出来
 
----------

![](https://i.imgur.com/nbbd2mE.png)

  
 

----------

### 总结

 - 项目中走的**弯路**
   - 数据集下载下来后，将数据上传至tinymind平台后，直接用第八周的目标检测代码训练，输出结果自然也是错误的，输出很多杂乱boxes。原因也是因为预处理数据中没有位置信息，实际上在项目介绍中有很清楚的提到：数据集为分类数据

    > 解决方法 ：网上有很多共享的数据集，我这里用的pascal voc2012的数据集作为检测模型训练所用数据。
  
  - RuntimeError: module compiled against API version 0xc but this version of numpy is 0xb
ImportError: numpy.core.multiarray failed to import

    >解决方法：bug是说numpy导入失败，原因是环境PYTHON版本和GitHub仓库代码版本不匹配，将tinymind 运行环境降为tensorflow1.4框架
  - 分类训练刚开始用的0.1的学习率，结果在训练5个小时的就early—stop，并且准确率只有0.03
    >解决办法：学习率步长过大，导致loss越过局部最优，尝试将学习率降低
  - 在虚拟机上安装gpu环境，反复安装了1天，未果
    >解决办法：虚拟机的显卡是虚拟的，不能使用CUDA
  - 用训练模型在本地虚拟机inference的时候，终端总是killed，没有其他报错提示
    >解决办法：可能是虚拟机内存不足，尝试将更大的内存分配给虚拟机
  - inference分类结果输出要卡半天
    >解决办法：重新看了课程视频，发现是用的model的动态节点，系统是重新计算的各个训练节点图，所以很慢，应该将模型导出冻结，这样可以加速模型的载入

 - **项目不足与改进思想**
  - ![](https://i.imgur.com/aPGwZSh.png) 
  - 可以看到图上的检测box高出了实际物体位置很多，
  - ![](https://i.imgur.com/5R7KUKj.png)
  - 图中是两辆车，而模型只检测到一个
  - 检测模型精度提高空间很大，应该尝试用imagenet数据集再训练一次

- **心得体会**
  - 这个项目主要就是将第七周和第八周的作业结合起来，重新复习下所学，其中第七周利用slim框架来对物体进行分类识别。第八周，使用object-detection框架来进行物体的检测和识别。
  - 大致包括了数据处理，模型训练，效果检验三个部分：
   - 数据处理算是重中之重，数据质量决定了最终训练结果的上限，人去学习也一样，学习资料是错误的，那学习结果不会好到哪里。
   - 模型训练在设置好初始化参数后，更多的是机器的循环运行，好的代码可以从输出log中监督到训练情况
   - 多种场景依次测试，可以对模型运行环境起到检验作用
### 使用说明
 - ckpt目录下保存有模型权重冻结图、model.ckpt文件
 - 在inference.py中代码11行：
  - `from utils import vis as vis_util`，将可视化代码vis.py文件放在util目录下
 - 执行图片检测分类代码
 - `python inference.py --test_img_path=/path_to_picture/test.jpg`